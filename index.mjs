import { readFileSync } from 'fs';

export class ExecutionPointError extends Error {
    constructor() {
        super('Execution should not reach this point');
    }
}

export class DummyError extends Error {
    constructor() {
        super('Thrown on purpose by a unit test.');
    }
}

export class ErrorTracker {
    constructor() {
        this.errors = [];
    }

    runAndTrack(fn) {
        const {errors} = this;
        try { fn(); } catch(e) { errors.push(e); throw e; };
    }

    printErrors(indent = 8) {
        const {errors} = this;
        while (errors.length) {
            const error = errors.shift();
            let {explain, message} = error;
            message = explain instanceof Function
                ? explain.call(error)
                : message;
            console.error(`${' '.repeat(indent)}${message}`);
        }
    }
}

export function loadJson(path) {
    const content = readFileSync(path);
    return JSON.parse(content);
}